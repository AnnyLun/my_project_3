2.1 Вам потрібно додати нове домашнє завдання в новий репозиторій на Gitlab. 
- додаємо на сайт GitLab новий репозиторій:
* на сайті https://gitlab.com/ тиснемо додати New project/repository
* в GitBush знаходимо корневу папку за допомогою "сd" - тиснемо git init - далі git remote add origin URL нашого репозиторія - далі git add . - git commit -m - далі git push -u origin master
2.2 Вам потрібно додати існуюче домашнє завдання в новий репозиторій на Gitlab.
- додаємо на сайт GitLab новий репозиторій "Create project"
- склонувати новий репозиторій на комп'ютер git clone URL папки
* в GitBush знаходимо корневу папку за допомогою "сd"
* git add .
* git commit -m ''
* git push